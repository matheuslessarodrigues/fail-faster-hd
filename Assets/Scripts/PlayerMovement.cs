﻿using UnityEngine;

public sealed class PlayerMovement : MonoBehaviour
{
	public Rigidbody2D playerRigidbody;
	public float moveVelocity = 1.0f;
	public float jumpImpulse = 1.0f;
	public Rect groundCheckRect = new Rect( 0.0f, 0.0f, 14.0f, 0.1f );
	public LayerMask groundLayer;

	[Header( "View" )]
	public SpriteRenderer playerSpriteRenderer;

	private void Update()
	{
		if( GameController.instance.Paused )
			return;

		if( Input.GetButtonDown( "Jump" ) && Physics2D.OverlapBox( playerRigidbody.position + groundCheckRect.position, groundCheckRect.size, 0.0f, groundLayer ) )
			playerRigidbody.AddForce( new Vector2( 0.0f, jumpImpulse ), ForceMode2D.Impulse );
	}

	private void FixedUpdate()
	{
		if( GameController.instance.Paused )
			return;

		var moveInput = Input.GetAxis( "Horizontal" );

		var velocity = playerRigidbody.velocity;
		velocity.x = moveInput * moveVelocity;
		playerRigidbody.velocity = velocity;

		if( moveInput > 0.0f )
			playerSpriteRenderer.flipX = false;
		else if( moveInput < 0.0f )
			playerSpriteRenderer.flipX = true;
	}

	private void OnDrawGizmosSelected()
	{
		Gizmos.color = Color.yellow;
		Gizmos.DrawWireCube( ( Vector2 ) transform.position + groundCheckRect.position, groundCheckRect.size );
	}
}
