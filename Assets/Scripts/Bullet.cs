﻿using UnityEngine;

public sealed class Bullet : MonoBehaviour
{
	public Rigidbody2D bulletRigidbody;
	public Collider2D bulletCollider;
	public SpriteRenderer bulletSpriteRenderer;
	public ParticleSystem destroyFx;

	private void OnTriggerEnter2D( Collider2D other )
	{
		destroyFx.transform.SetParent( null, true );
		destroyFx.Play( false );

		Destroy( gameObject );
	}
}
