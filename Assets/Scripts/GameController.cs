﻿using UnityEngine;
using UnityEngine.SceneManagement;

public sealed class GameController : MonoBehaviour
{
	public static GameController instance;

	public UI ui;

	public float startTimestamp;
	public int deathCount;

	private bool paused = false;
	public bool Paused
	{
		get { return paused; }
		set
		{
			paused = value;
			Physics2D.autoSimulation = !paused;
		}
	}

	public void EndGame()
	{
		Paused = true;
		ui.ShowEndGame( Mathf.CeilToInt( Time.time - startTimestamp ), deathCount );
		enabled = true;
	}

	private void Awake()
	{
		instance = this;
		startTimestamp = Time.time;

		enabled = false;
	}

	private void Update()
	{
		if( Input.anyKeyDown )
			SceneManager.LoadScene( SceneManager.GetActiveScene().buildIndex );
	}
}
