﻿using System.Collections;
using UnityEngine;

public sealed class PlayerDeath : MonoBehaviour
{
	public Rigidbody2D playerRigidbody;
	public SpriteRenderer playerSpriteRenderer;
	public GameObject playerCorpsePrefab;
	public string deadlyTag = "Deadly";
	public float respawnDelay = 1.0f;

	[Header( "View" )]
	public ParticleSystem deathFxPrefab;

	private Vector2 initialPosition;
	private ParticleSystem deathFx;

	private void KillPlayer()
	{
		if( !playerRigidbody.simulated )
			return;

		deathFx.transform.localPosition = playerRigidbody.position;
		deathFx.gameObject.SetActive( true );
		deathFx.Play( false );

		Instantiate( playerCorpsePrefab, playerRigidbody.position, Quaternion.identity );

		playerRigidbody.simulated = false;
		playerSpriteRenderer.enabled = false;

		GameController.instance.deathCount += 1;

		StartCoroutine( RespawnCoroutine() );
	}

	private void Start()
	{
		initialPosition = playerRigidbody.position;

		deathFx = Instantiate( deathFxPrefab );
		deathFx.gameObject.SetActive( false );
	}

	private void Update()
	{
		if( playerRigidbody.position.y < 0.0f )
			KillPlayer();
	}

	private void OnTriggerEnter2D( Collider2D other )
	{
		if( !other.CompareTag( deadlyTag ) )
			return;

		KillPlayer();
	}

	private IEnumerator RespawnCoroutine()
	{
		yield return new WaitForSeconds( respawnDelay );

		transform.position = initialPosition;

		playerRigidbody.simulated = true;
		playerRigidbody.position = initialPosition;
		playerRigidbody.velocity = Vector2.zero;

		playerSpriteRenderer.enabled = true;
	}
}
