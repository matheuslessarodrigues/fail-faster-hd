﻿using UnityEngine;
using UnityEngine.SceneManagement;

public sealed class MapEditor : MonoBehaviour
{
	public static MapEditor instance;
	public static bool isEditing = false;

	public Map map;
	public GameObject cursorPrefab;

	private GameObject palletRoot;
	private GameObject[] tilePallet;
	private int currentTileIndex = 0;
	private Camera mainCamera;

	public int debugMapX;
	public int debugMapY;

	public void ToggleEditMode()
	{
		if( isEditing )
			map.SaveMap();

		isEditing = !isEditing;
		SceneManager.LoadScene( SceneManager.GetActiveScene().buildIndex );
	}

	private void Awake()
	{
		instance = this;

		palletRoot = Instantiate( cursorPrefab );
		tilePallet = new GameObject[map.tilePrefabs.Length];
		for( int i = 0; i < tilePallet.Length; i++ )
		{
			var tilePrefab = map.tilePrefabs[i];
			tilePallet[i] = Instantiate( tilePrefab, palletRoot.transform );
			tilePallet[i].SetActive( false );
		}

		tilePallet[currentTileIndex].SetActive( isEditing );
		palletRoot.SetActive( isEditing );

		mainCamera = Camera.main;
		GameController.instance.Paused = isEditing;
	}

	private void Update()
	{
		if( !isEditing )
			return;

		// Select
		if( Input.GetButtonDown( "NextTilePallet" ) )
			SelectTileFromIndex( currentTileIndex + 1 );
		else if( Input.GetButtonDown( "PreviousTilePallet" ) )
			SelectTileFromIndex( currentTileIndex - 1 );

		// Move
		var mouseWorldPosition = mainCamera.ScreenToWorldPoint( Input.mousePosition );
		var mouseMapX = Mathf.FloorToInt( mouseWorldPosition.x / map.tileSize );
		var mouseMapY = Mathf.FloorToInt( mouseWorldPosition.y / map.tileSize );

		mouseMapX = Mathf.Clamp( mouseMapX, 0, map.mapWidth - 1 );
		mouseMapY = Mathf.Clamp( mouseMapY, 0, map.mapHeight - 1 );

		debugMapX = mouseMapX;
		debugMapY = mouseMapY;

		palletRoot.transform.position = new Vector3( mouseMapX + 0.5f, mouseMapY + 0.5f, 0.0f ) * map.tileSize;

		// Replace
		if( Input.GetMouseButton( 0 ) && GameController.instance.ui.uiGroup.alpha == 0.0f )
		{
			int mapIndex = mouseMapY * map.mapWidth + mouseMapX;
			map.ReplaceAt( ( byte ) mapIndex, ( byte ) currentTileIndex );
		}
	}

	private void SelectTileFromIndex( int index )
	{
		tilePallet[currentTileIndex].SetActive( false );
		currentTileIndex = ( index + tilePallet.Length ) % tilePallet.Length;
		tilePallet[currentTileIndex].SetActive( true );
	}
}
