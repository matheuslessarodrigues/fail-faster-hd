﻿using UnityEngine;
using System.Text;

public static class MapHelper
{
	public static string ArrayToString<T>( T[] array )
	{
		var sb = new StringBuilder();

		sb.Append( "[" );
		foreach( var element in array )
		{
			sb.Append( element.ToString() );
			sb.Append( ", " );
		}

		sb.Remove( sb.Length - 2, 2 );
		sb.Append( "]" );

		return sb.ToString();
	}

	public static string EncodeMap( byte[] tileIndices )
	{
		if( tileIndices == null )
			return "";

		var compressedMap = new byte[tileIndices.Length / 2];
		for( int i = 0; i < compressedMap.Length; i++ )
			compressedMap[i] = ( byte ) ( ( tileIndices[i * 2] << 4 ) | ( tileIndices[i * 2 + 1] ) );

		return System.Convert.ToBase64String( compressedMap );
	}

	public static byte[] DencodeMap( string encodedMap )
	{
		if( string.IsNullOrEmpty( encodedMap ) )
			return null;

		byte[] compressedMap;
		try
		{
			compressedMap = System.Convert.FromBase64String( encodedMap );
		}
		catch
		{
			return null;
		}

		if( compressedMap == null )
			return null;

		var tileIndices = new byte[compressedMap.Length * 2];
		for( int i = 0; i < compressedMap.Length; i++ )
		{
			byte b = compressedMap[i];
			tileIndices[i * 2] = ( byte ) ( ( b >> 4 ) & 0xF );
			tileIndices[i * 2 + 1] = ( byte ) ( b & 0xF );
		}

		return tileIndices;
	}
}
