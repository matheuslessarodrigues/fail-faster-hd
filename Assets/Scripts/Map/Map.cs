﻿using UnityEngine;
using UnityEngine.SceneManagement;

public sealed class Map : MonoBehaviour
{
	private static string savedMap;
	private static bool madeFirstLoad = false;

	[Header( "Tileset" )]
	public GameObject[] tilePrefabs;
	public byte emptyTileIndex = 0;
	public byte playerTileIndex = 1;
	public byte goalTileIndex = 2;
	public byte groundTileIndex = 3;

	[Header( "Map" )]
	public int tileSize = 16;
	public int mapWidth = 16;
	public int mapHeight = 14;

	private GameObject[] tiles;
	private byte[] tileIndices;

	public string GetEncodedMap()
	{
		return MapHelper.EncodeMap( tileIndices );
	}

	public void SaveMap()
	{
		savedMap = MapHelper.EncodeMap( tileIndices );
	}

	public bool LoadMap( byte[] map )
	{
		if( map.Length < mapWidth * mapHeight )
			return false;

		tileIndices = map;
		tiles = new GameObject[mapWidth * mapHeight];

		for( byte i = 0; i < tiles.Length; i++ )
			ReplaceAt( i, tileIndices[i] );

		return true;
	}

	public void ReplaceAt( byte mapIndex, byte tileIndex )
	{
		if( tileIndex == playerTileIndex || tileIndex == goalTileIndex )
		{
			for( byte i = 0; i < tileIndices.Length; i++ )
			{
				if( tileIndices[i] == tileIndex )
					DeleteAt( i );
			}
		}

		DeleteAt( mapIndex );

		tileIndices[mapIndex] = tileIndex;
		if( tileIndex != emptyTileIndex )
		{
			var position = new Vector3(
				( mapIndex % mapWidth + 0.5f ) * tileSize,
				( mapIndex / mapWidth + 0.5f ) * tileSize,
				0.0f
			);

			tiles[mapIndex] = Instantiate( tilePrefabs[tileIndex], position, Quaternion.identity );
		}
	}

	private void DeleteAt( byte mapIndex )
	{
		if( tiles[mapIndex] != null )
		{
			Destroy( tiles[mapIndex] );
			tiles[mapIndex] = null;
		}

		tileIndices[mapIndex] = emptyTileIndex;
	}

	private string GetEncodedMapFromUrl( string url )
	{
		var separator = url.IndexOf( "?" );
		if( separator < 0 || url.Length <= separator + 1 )
			return null;

		var allArgs = url.Substring( separator + 1 );
		var args = allArgs.Split( '&' );

		foreach( var arg in args )
		{
			const string key = "map=";
			if( arg.StartsWith( key ) )
			{
				var value = arg.Substring( key.Length );
				var encodedMap = WWW.UnEscapeURL( value );
				return encodedMap;
			}
		}

		return null;
	}

	private void Start()
	{
		if( !madeFirstLoad )
		{
			madeFirstLoad = true;
			savedMap = Application.platform == RuntimePlatform.WebGLPlayer ?
				GetEncodedMapFromUrl( Application.absoluteURL ) :
				GUIUtility.systemCopyBuffer;
		}

		var loaded = false;

		{
			var map = MapHelper.DencodeMap( savedMap );
			if( map != null )
				loaded = LoadMap( map );
		}

		if( !loaded )
		{
			var map = new byte[mapWidth * mapHeight];
			for( int i = 0; i < map.Length; i++ )
				map[i] = emptyTileIndex;

			for( int i = 0; i < mapWidth; i++ )
				map[i] = groundTileIndex;

			map[mapWidth + 1] = playerTileIndex;
			map[mapWidth * 2 - 1] = goalTileIndex;

			map[map.Length - 1] = groundTileIndex;

			MapHelper.EncodeMap( map );
			LoadMap( map );
		}
	}
}
