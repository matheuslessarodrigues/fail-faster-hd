﻿using UnityEngine;
using UnityEngine.SceneManagement;

public sealed class Goal : MonoBehaviour
{
	private void OnTriggerEnter2D( Collider2D other )
	{
		GameController.instance.EndGame();
	}

	private void Update()
	{
		if( GameController.instance.Paused )
			return;

		if( Input.GetButtonDown( "Reset" ) )
			SceneManager.LoadScene( SceneManager.GetActiveScene().buildIndex );
	}
}
