﻿using UnityEngine;

public sealed class Gargoyle : MonoBehaviour
{
	public Collider2D gargoyleCollider;
	public SpriteRenderer gargoyleSpriteRenderer;
	public Bullet bulletPrefab;
	public float firePeriod = 1.0f;
	public float fireVelocity = 10.0f;

	private float lastFireTimestamp = 0.0f;

	private void Update()
	{
		if( GameController.instance.Paused )
			return;

		float currentTime = Time.time;
		if( lastFireTimestamp + firePeriod < currentTime )
		{
			lastFireTimestamp = currentTime;

			var bullet = Instantiate( bulletPrefab, transform.position, transform.rotation );
			var velocity = gargoyleSpriteRenderer.flipX ? fireVelocity : -fireVelocity;
			bullet.bulletRigidbody.velocity = transform.right * velocity;

			Physics2D.IgnoreCollision( bullet.bulletCollider, gargoyleCollider );
		}
	}
}
