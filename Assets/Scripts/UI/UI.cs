﻿using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public sealed class UI : MonoBehaviour
{
	public CanvasGroup uiGroup;

	[Header( "Web" )]
	public string baseUrl;

	[Header( "Game Over" )]
	public CanvasGroup gameOverGroup;
	public TextMeshProUGUI totalTimeLabel;
	public TextMeshProUGUI deathCountLabel;

	[Header( "Menu" )]
	public CanvasGroup menuGroup;
	public Button editLevelButton;
	public Button playLevelButton;
	public TextMeshProUGUI copyMapLabel;
	public TextMeshProUGUI copyMapUrlLabel;
	public TextMeshProUGUI copiedInfoLabel;

	[Header( "InGame" )]
	public CanvasGroup editModeGroup;

	public float animationTime = 0.1f;

	public void ShowEndGame( int totalSeconds, int deathCount )
	{
		totalTimeLabel.text = string.Format( totalTimeLabel.text, totalSeconds );
		deathCountLabel.text = string.Format( deathCountLabel.text, deathCount );

		gameOverGroup.alpha = 1.0f;
		menuGroup.alpha = 0.0f;

		StartCoroutine( FadeUiInCoroutine() );
	}

	public void ShowMenu()
	{
		GameController.instance.Paused = true;
		gameOverGroup.alpha = 0.0f;
		menuGroup.alpha = 1.0f;

		copiedInfoLabel.enabled = false;

		StartCoroutine( FadeUiInCoroutine() );
	}

	public void OnResume()
	{
		if( !MapEditor.isEditing )
			GameController.instance.Paused = false;
		uiGroup.alpha = 0.0f;
	}

	public void OnToggleEditLevel()
	{
		MapEditor.instance.ToggleEditMode();
	}

	public void OnCopyMapToClipboard()
	{
		GUIUtility.systemCopyBuffer = Application.platform == RuntimePlatform.WebGLPlayer ?
			GetMapUrl() :
			MapEditor.instance.map.GetEncodedMap();

		copiedInfoLabel.enabled = true;
	}

	private string GetMapUrl()
	{
		var encodedMap = MapEditor.instance.map.GetEncodedMap();
		encodedMap = WWW.EscapeURL( encodedMap );
		return string.Format( "{0}?map={1}", baseUrl, encodedMap );
	}

	private IEnumerator FadeUiInCoroutine()
	{
		for( var time = 0.0f; time < animationTime; time += Time.deltaTime )
		{
			var t = time / animationTime;
			uiGroup.alpha = t;
			yield return null;
		}

		uiGroup.alpha = 1.0f;
	}

	private void Awake()
	{
		uiGroup.alpha = 0.0f;

		editLevelButton.gameObject.SetActive( !MapEditor.isEditing );
		playLevelButton.gameObject.SetActive( MapEditor.isEditing );

		var isWeb = Application.platform == RuntimePlatform.WebGLPlayer;
		copyMapLabel.gameObject.SetActive( !isWeb );
		copyMapUrlLabel.gameObject.SetActive( isWeb );

		editModeGroup.alpha = MapEditor.isEditing ? 1.0f : 0.0f;
	}

	private void Update()
	{
		if( Input.GetKeyDown( KeyCode.Escape ) )
		{
			if( uiGroup.alpha > 0.0f )
				OnResume();
			else
				ShowMenu();
		}
	}
}
